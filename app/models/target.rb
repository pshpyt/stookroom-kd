class Target < ActiveRecord::Base
  has_many :transactions
  default_scope { order(name: :asc) }
  
end
