# lib/tasks/db.rake
namespace :db do
  desc "Dumps the database to db/backups/"
  task :dump => :environment do
  cmd = nil
  
  FileUtils.mkdir_p 'db/backups/'
  
  with_config do |app, host, db, user, password|
    cmd = "PGPASSWORD='kidsclub' pg_dump --host #{host} --username #{user}   --verbose --clean --no-owner --no-acl --format=c #{db} > #{Rails.root}/db/backups/#{Time.now.strftime("%d%b_%Y")}.dump"
  end
  puts cmd
  exec cmd
  end

  desc "Restores the database dump: bundle exec rake db:restore dump_name=..."
  task :restore => :environment do
  cmd = nil
  with_config do |app, host, db, user, password|
    cmd = "PGPASSWORD='kidsclub'  pg_restore --verbose --host #{host} --username #{user} --clean --no-owner --no-acl --dbname #{db} #{Rails.root}/db/backups/#{ENV['dump_name']}.dump"
    #cmd = "pg_restore --verbose --host #{host} --username #{user} --clean --no-owner --no-acl --dbname #{db} #{Rails.root}/db/#{app}.dump"
  end
  Rake::Task["db:drop"].invoke
  Rake::Task["db:create"].invoke
  puts cmd
  exec cmd
  end

  private

  def with_config
      yield Rails.application.class.parent_name.underscore,
      ActiveRecord::Base.connection_config[:host],
      ActiveRecord::Base.connection_config[:database],
      ActiveRecord::Base.connection_config[:username]
  end

end
