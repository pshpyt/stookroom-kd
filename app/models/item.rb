class Item < ActiveRecord::Base
  acts_as_paranoid
  has_paper_trail
  default_scope { order(name: :asc) }
  paginates_per 50
  has_many :transactions
  before_save :update_total 
  belongs_to :category
  counter_culture :category
  counter_culture :category, :column_name => 'total_cost', :delta_column => 'total'
   #scope :danger, -> { where("amount < ?", red_line) }
  composed_of :price,
              :class_name => 'Money',
              :mapping => %w(price cents),
              :converter => Proc.new { |value| Money.new(value.to_s.delete!(',')) }
  
  composed_of :total,
              :class_name => 'Money',
              :mapping => %w(total cents),
              :converter => Proc.new { |value| Money.new(value.to_s.delete!(',')) }
  
  def get_total
   self.amount*self.price.cents
    
  end
  
  def balance
    self.amount + transactions.where( :action_type => "Out").sum(:amount) - transactions.where( :action_type => "In").sum(:amount)  
  end
  
  def last_income
    transactions.where(:action_type => 'In').first.created_at.strftime("%d/%m/%Y") rescue created_at.strftime("%d/%m/%Y")
  end

  def self.total_sum
    sum(:total)
  end
  
  private
  def update_total
    puts "--------------- #{self.id} ----"
    puts "get_total=#{get_total}"
    self.total = Money.new("#{get_total}")
    
   # if self.price >0 and self.amount > 0 
   #   binding.pry
   # end
   
   # puts self.total.cents
  end
puts "========================================"
end
