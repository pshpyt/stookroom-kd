Stockroom::Application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  resources :categories do 
    resources :items
  end
  resources :items do
    member do
      get :reload
    end
  end
  resources :targets
  resources :transactions do
      member do
        post :decline
      end
    
    collection do
      get 'report_page'
      get 'report_by_target'
    end
  end
  root "items#index"
  get "home", to: "pages#home", as: "home"
  get "danger", to: "items#danger", as: "danger"
  get "wrong_balance", to: "items#wrong_balance", as: "wrong_balance"
  get "inside", to: "pages#inside", as: "inside"
  get "report", to: "pages#report", as: "report"
  get "/contact", to: "pages#contact", as: "contact"
  post "/emailconfirmation", to: "pages#email", as: "email_confirmation"

  get "posts", to: "pages#posts", as: "posts"
  get "posts/:id", to: "pages#show_post", as: "post"
  devise_for :users

  namespace :admin do
    root "base#index"
    resources :users
    get "posts/drafts", to: "posts#drafts", as: "posts_drafts"
    get "posts/dashboard", to: "posts#dashboard", as: "posts_dashboard"
    resources :posts
  end

end
