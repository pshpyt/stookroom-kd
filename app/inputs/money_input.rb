# app/inputs/money_input.rb

class MoneyInput < SimpleForm::Inputs::StringInput
# def input(wrapper_options = nil)
#   format("&euro; %s,%s" input_major, input_minor).html_safe
# end

# def label_html_options
#   super.merge(for: "#{@builder.object_name} _#{attribute_name}_1i")
# end

# private
def input_major
  template.text_field_tag(
    "#{@builder.object_name}[#{attribute_name}(1i)]",
    @builder.object.send(attribute_name).major,
    id: "#{@builder.object_name}_#{attribute_name}_1i"
  )
end

def input_minor
  template.text_field_tag(
    "#{@builder.object_name}[#{attribute_name}(2i)]",
    @builder.object.send(attribute_name).minor,
    id: "#{@builder.object_name}_#{attribute_name}_2i"
  )
end
end
